package com.miq.runner;

import com.intuit.karate.cucumber.CucumberRunner;
import com.intuit.karate.cucumber.KarateStats;
import com.miq.heimdall.HeimdallReporting;
import com.miq.slack.SlackChannelInfo;
import cucumber.api.CucumberOptions;
import org.junit.Test;

import java.io.IOException;
import java.util.concurrent.ExecutionException;

import static org.junit.Assert.assertTrue;

@CucumberOptions(features = {"classpath:features"}, tags = {"@AMP-2884"})
public class RunnerTest {
    private static final String SLACK_TOKEN = "xoxb-4735837518-706843868213-TOXgsJJwZafyMICeX3D563Dp";
    @Test
    public void testParallel() throws InterruptedException, ExecutionException, NoSuchFieldException, IllegalAccessException,
            IOException {
        String karateOutputPath = "target/cucumber-html-reports";
        KarateStats stats = CucumberRunner.parallel(getClass(), 5, karateOutputPath);
        SlackChannelInfo slackChannelInfo = SlackChannelInfo.builder().channelName("amnet-test-report")
                .token(SLACK_TOKEN).isNotifySlack(true).build();
        HeimdallReporting.generateReportAndNotifySlack(karateOutputPath, slackChannelInfo, "AMP");
        assertTrue("there are scenario failures", stats.getFailCount() == 0);
    }
}