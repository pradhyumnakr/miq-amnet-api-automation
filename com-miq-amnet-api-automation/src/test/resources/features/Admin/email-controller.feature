@regression2
Feature: Email Controller

  Background:
    * header Content-Type = 'application/json'
    #* header Authorization = 'Bearer eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiaW5maW5pdHkiXSwidXNlcl9uYW1lIjoicHJhZGh5dW1uYS5rQG1pcWRpZ2l0YWwuY29tIiwic2NvcGUiOlsiQURNSU5fREVMRVRFIiwiT1BUSU1JWkVfREVMRVRFIiwiTUFOQUdFX0RFTEVURSIsIkRJU0NPVkVSX0RFTEVURSJdLCJleHAiOjE1NjI5MjQ0ODgsImF1dGhvcml0aWVzIjpbIklORklOSVRZX0FNTkVUX05FV19VU0VSIiwiSU5GSU5JVFlfQU1ORVRfU1VQRVJfQURNSU4iXSwianRpIjoiNjQyNzA2YWEtMmUwZi00NmE3LTgwNzgtZTI0MzlmYzUxZjJiIiwiY2xpZW50X2lkIjoiaW5maW5pdHkifQ.IC7LWmupz8fV-qEH1QLtVmYBUMNnoMS-yjpYdUZNVcHa7t5eGxYjY2IFtdAW49LaWlX3QJLf4O84gyanmSZWQQ5eC0_U7u1542YCwPrKemHZPZQqEP1ZIv5GvYygYkxczqNszOdDMMtxap5Mfaidd7cQsWJl4aAxNvBXrZBj3IFbBRNKnjERrHBn25UhNQsqjiI9dZ1-bwcdchs_lvAoaesbdvtCW9Gj5wqWNdrRKsz1Ud_0n4nWEEUvfjWBqk4zM1WMpi_bfNNxbDD8Wln8bFwTxyYxD7D2vLuNr2KN5TEmX8l0TbUi5M07mL8ljm2XTe41-ksTsJSi4WfkZnX__w'
    * configure ssl = true

 #Waste
#  Scenario Outline: Making Post call for Email-feedback
#    Given url 'feedback'
#    And request {}
##    And param file = "<filename>"
#    And param feedbackText = "<feedback>"
#    And param from = "<from>"
#    When method Post
#    Then status 200
#    * print response
#    Examples:
#      | filename | feedback | from  |
#      | abc      | Good     | abcac |
#      | xyz      | Bad      | asqw  |

  @AMP-2856
  Scenario: Making post call for new UserEmail
    Given url 'http://localhost:8080/admin-web-service/email/newUsersEmail'
    And request [ { "email": "pradhyumnakr@gmail.com", "firstName": "Pradhyumna", "lastName": "K R", "message": "Welcome to Amnet", "password": "qwerty" } ]
    And param adminName = "pradhyumna.k@miqdigital.com"
    When method Post
    Then status 200
    * print response
    * match response == "Email sent successfully to admin and new users"

  @AMP-2857
  Scenario: Making post call for new UserEmail
    Given url 'http://localhost:8080/admin-web-service/email/resetPasswordEmail'
    And request { "email": "pradhyumnakr@gmail.com", "firstName": "pradhyumna", "lastName": "K R", "message": "Welcome", "password": "qwerty" }
    When method Post
    Then status 200
    * print response
    * match response == "Email sent successfully for reset password"





