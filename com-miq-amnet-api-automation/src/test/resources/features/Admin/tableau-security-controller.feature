@regression
Feature: tableau-security-controller

  Background:
   * header Content-Type = 'application/json'
   * configure ssl = true
 @AMP-2113
 Scenario: Making Get call to the tableau-security-controller users
   Given url 'http://localhost:8080/admin-web-service/tableau-security/users'
   When method Get
   Then status 200
   * print response
   * match response contains  [ "#string"]