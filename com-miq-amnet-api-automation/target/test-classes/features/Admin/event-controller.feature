@regression23
Feature: event Controller

  Background:
    * header Content-Type = 'application/json'
    #* header Authorization = 'Bearer eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiaW5maW5pdHkiXSwidXNlcl9uYW1lIjoicHJhZGh5dW1uYS5rQG1pcWRpZ2l0YWwuY29tIiwic2NvcGUiOlsiQURNSU5fREVMRVRFIiwiT1BUSU1JWkVfREVMRVRFIiwiTUFOQUdFX0RFTEVURSIsIkRJU0NPVkVSX0RFTEVURSJdLCJleHAiOjE1NjI5MjQ0ODgsImF1dGhvcml0aWVzIjpbIklORklOSVRZX0FNTkVUX05FV19VU0VSIiwiSU5GSU5JVFlfQU1ORVRfU1VQRVJfQURNSU4iXSwianRpIjoiNjQyNzA2YWEtMmUwZi00NmE3LTgwNzgtZTI0MzlmYzUxZjJiIiwiY2xpZW50X2lkIjoiaW5maW5pdHkifQ.IC7LWmupz8fV-qEH1QLtVmYBUMNnoMS-yjpYdUZNVcHa7t5eGxYjY2IFtdAW49LaWlX3QJLf4O84gyanmSZWQQ5eC0_U7u1542YCwPrKemHZPZQqEP1ZIv5GvYygYkxczqNszOdDMMtxap5Mfaidd7cQsWJl4aAxNvBXrZBj3IFbBRNKnjERrHBn25UhNQsqjiI9dZ1-bwcdchs_lvAoaesbdvtCW9Gj5wqWNdrRKsz1Ud_0n4nWEEUvfjWBqk4zM1WMpi_bfNNxbDD8Wln8bFwTxyYxD7D2vLuNr2KN5TEmX8l0TbUi5M07mL8ljm2XTe41-ksTsJSi4WfkZnX__w'
    * configure ssl = true

  @AMP-2858
  Scenario Outline: Making get call for event login
    Given url 'http://localhost:8080/admin-web-service/api/v1.0/event/login'
    And param userName = "<user_name>"
    When method Get
    Then status 200
    * print response
    * match response == { "userName": "#string", "loginTimestamp": "#present", "logoutTimestamp": "#present", "sessionStatus": "#present", "sessionId": "#present", "featuresClickEvents": "#present" }
    Examples:
      | user_name                   |
      | pradhyumna.k@miqdigital.ocm |
      | pradhyumna.k@miqdigital.com |

  @AMP-2873
  Scenario Outline: Checking Login and Logout by making a post and put call for login and logout
    Given url 'http://localhost:8080/admin-web-service/api/v1.0/event/login'
    And request {}
    And param userName = "<user_name>"
    When method Post
    Then status 201
    * print response
    * match response == { "userName": "#string", "loginTimestamp": "#present", "logoutTimestamp": "#present", "sessionStatus": "#present", "sessionId": "#present", "featuresClickEvents": "#present" }
    * def session_id = response.sessionId
    * print session_id

    Given url 'http://localhost:8080/admin-web-service/api/v1.0/event/logout'
    And request {}
    And param userName = "<user_name>"
    When method Put
    Then status 201
    * print 'printing here logout response' + response[0].sessionId
    * match response[0].sessionId == session_id

    Examples:
      | user_name                   |
      | pradhyumna.k@miqdigital.com |

   @AMP-2884
  Scenario Outline: Making PUT call to Event logout
    Given url 'http://localhost:8080/admin-web-service/api/v1.0/event/logout'
    And request {}
    And param userName = "<user_name>"
    When method Put
    Then status 201
    * print response
    * match response == []
    Examples:
      | user_name                   |
      | pradhyumna.k@miqdigital.com |

