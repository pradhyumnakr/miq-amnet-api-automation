@regression
Feature: event Controller

  Background:
    * header Content-Type = 'application/json'
    #* header Authorization = 'Bearer eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiaW5maW5pdHkiXSwidXNlcl9uYW1lIjoicHJhZGh5dW1uYS5rQG1pcWRpZ2l0YWwuY29tIiwic2NvcGUiOlsiQURNSU5fREVMRVRFIiwiT1BUSU1JWkVfREVMRVRFIiwiTUFOQUdFX0RFTEVURSIsIkRJU0NPVkVSX0RFTEVURSJdLCJleHAiOjE1NjI5MjQ0ODgsImF1dGhvcml0aWVzIjpbIklORklOSVRZX0FNTkVUX05FV19VU0VSIiwiSU5GSU5JVFlfQU1ORVRfU1VQRVJfQURNSU4iXSwianRpIjoiNjQyNzA2YWEtMmUwZi00NmE3LTgwNzgtZTI0MzlmYzUxZjJiIiwiY2xpZW50X2lkIjoiaW5maW5pdHkifQ.IC7LWmupz8fV-qEH1QLtVmYBUMNnoMS-yjpYdUZNVcHa7t5eGxYjY2IFtdAW49LaWlX3QJLf4O84gyanmSZWQQ5eC0_U7u1542YCwPrKemHZPZQqEP1ZIv5GvYygYkxczqNszOdDMMtxap5Mfaidd7cQsWJl4aAxNvBXrZBj3IFbBRNKnjERrHBn25UhNQsqjiI9dZ1-bwcdchs_lvAoaesbdvtCW9Gj5wqWNdrRKsz1Ud_0n4nWEEUvfjWBqk4zM1WMpi_bfNNxbDD8Wln8bFwTxyYxD7D2vLuNr2KN5TEmX8l0TbUi5M07mL8ljm2XTe41-ksTsJSi4WfkZnX__w'
    * configure ssl = true

  @AMP-2859
  Scenario Outline: Making Get call for ldap-user with valid users
    Given url 'http://localhost:8080/admin-web-service/api/v1.0/ldap/user'
    And param email = "<user_name>"
    When method Get
    Then status 200
    * print response
    * match response == { "firstName": "#string ", "lastName": "#string", "password": "#present", "email": "#string", "message": "#present" }
    Examples:
      | user_name                   |
      | pradhyumna.k@miqdigital.com |
      | subhanvit@gmail.com         |

  @AMP-2860
  Scenario Outline: Making Get call for ldap-user with invalid users
    Given url 'http://localhost:8080/admin-web-service/api/v1.0/ldap/user'
    And param email = "<user_name>"
    When method Get
    Then status 409
    * print response
    * match response == { "timestamp": "#present", "message": "Could not find user with email <user_name>", "details": "uri=/admin-web-service/api/v1.0/ldap/user" }
    Examples:
      | user_name                   |
      | pradhyumna.k@miqdigital.ocm |
      | subhanvit@gmail.ocm         |



  @AMP-2861
  Scenario: Making Get call to the tableau-security-controller users
    Given url 'http://localhost:8080/admin-web-service/api/v1.0/ldap/user-roles'
    When method Get
    Then status 200
    * print response
    * match  response contains  [ { "user": { "firstName": "#present", "lastName": "#present", "password": "#present", "email": "#string", "message": "#present" }, "roles": [ { "role": "#present", "addUser": "#present" } ] }]


  @AMP-2862
  Scenario: Making Get call to the tableau-security-controller users
    Given url 'http://localhost:8080/admin-web-service/api/v1.0/ldap/users'
    When method Get
    Then status 200
    * print response
    * match each response[*].users contains  [{ "firstName": "#present", "lastName": "#present", "password": "#present", "email": "#string", "message": "#present" }]