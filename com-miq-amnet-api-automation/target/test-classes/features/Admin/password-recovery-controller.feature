@regression
Feature: Password recovery Controller

  Background:
    * header Content-Type = 'application/json'
    #* header Authorization = 'Bearer eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiaW5maW5pdHkiXSwidXNlcl9uYW1lIjoicHJhZGh5dW1uYS5rQG1pcWRpZ2l0YWwuY29tIiwic2NvcGUiOlsiQURNSU5fREVMRVRFIiwiT1BUSU1JWkVfREVMRVRFIiwiTUFOQUdFX0RFTEVURSIsIkRJU0NPVkVSX0RFTEVURSJdLCJleHAiOjE1NjI5MjQ0ODgsImF1dGhvcml0aWVzIjpbIklORklOSVRZX0FNTkVUX05FV19VU0VSIiwiSU5GSU5JVFlfQU1ORVRfU1VQRVJfQURNSU4iXSwianRpIjoiNjQyNzA2YWEtMmUwZi00NmE3LTgwNzgtZTI0MzlmYzUxZjJiIiwiY2xpZW50X2lkIjoiaW5maW5pdHkifQ.IC7LWmupz8fV-qEH1QLtVmYBUMNnoMS-yjpYdUZNVcHa7t5eGxYjY2IFtdAW49LaWlX3QJLf4O84gyanmSZWQQ5eC0_U7u1542YCwPrKemHZPZQqEP1ZIv5GvYygYkxczqNszOdDMMtxap5Mfaidd7cQsWJl4aAxNvBXrZBj3IFbBRNKnjERrHBn25UhNQsqjiI9dZ1-bwcdchs_lvAoaesbdvtCW9Gj5wqWNdrRKsz1Ud_0n4nWEEUvfjWBqk4zM1WMpi_bfNNxbDD8Wln8bFwTxyYxD7D2vLuNr2KN5TEmX8l0TbUi5M07mL8ljm2XTe41-ksTsJSi4WfkZnX__w'
    * configure ssl = true

  @AMP-2863
  Scenario Outline: Making Get Call for login-status
    Given url 'http://localhost:8080/admin-web-service/api/v1.0/authorize/login-status'
    And param registeredEmail = "<user_name>"
    When method Get
    Then status 200
    * print response
    * match response == { "id": "#present", "infinityUser": "<user_name>", "tableauUser": "<user_name>", "password": "#present", "firstLogin": "#present" }
    Examples:
      | user_name                   |
      | pradhyumna.k@miqdigital.com |
      | pradhyumnakr@gmail.co       |

  @AMP-2864
  Scenario Outline: Checking the Password Recovery feature by Making Put call
    Given url 'http://localhost:8080/admin-web-service/api/v1.0/authorize/recover-password'
    And request {}
    And param registeredEmail = "<user_name>"
    When method Put
    Then status 200
    * print response
    * match response == { "firstName": "#present", "lastName": "#present", "password": "#present", "email": "#present", "message": "#present" }
    Examples:
      | user_name              |
      | pradhyumnakr@gmail.com |
      | pradhyumnakr@gmail.co  |
