@regression
Feature: Testing lookups

  Background:
    * header Content-Type = 'application/json'
    #* header Authorization = 'Bearer eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiaW5maW5pdHkiXSwidXNlcl9uYW1lIjoicHJhZGh5dW1uYS5rQG1pcWRpZ2l0YWwuY29tIiwic2NvcGUiOlsiQURNSU5fREVMRVRFIiwiT1BUSU1JWkVfREVMRVRFIiwiTUFOQUdFX0RFTEVURSIsIkRJU0NPVkVSX0RFTEVURSJdLCJleHAiOjE1NjI5MjQ0ODgsImF1dGhvcml0aWVzIjpbIklORklOSVRZX0FNTkVUX05FV19VU0VSIiwiSU5GSU5JVFlfQU1ORVRfU1VQRVJfQURNSU4iXSwianRpIjoiNjQyNzA2YWEtMmUwZi00NmE3LTgwNzgtZTI0MzlmYzUxZjJiIiwiY2xpZW50X2lkIjoiaW5maW5pdHkifQ.IC7LWmupz8fV-qEH1QLtVmYBUMNnoMS-yjpYdUZNVcHa7t5eGxYjY2IFtdAW49LaWlX3QJLf4O84gyanmSZWQQ5eC0_U7u1542YCwPrKemHZPZQqEP1ZIv5GvYygYkxczqNszOdDMMtxap5Mfaidd7cQsWJl4aAxNvBXrZBj3IFbBRNKnjERrHBn25UhNQsqjiI9dZ1-bwcdchs_lvAoaesbdvtCW9Gj5wqWNdrRKsz1Ud_0n4nWEEUvfjWBqk4zM1WMpi_bfNNxbDD8Wln8bFwTxyYxD7D2vLuNr2KN5TEmX8l0TbUi5M07mL8ljm2XTe41-ksTsJSi4WfkZnX__w'
    * configure ssl = true

  @AMP-2865
  Scenario Outline: Checking the Custom colour for the Custom Advertisers
    * def resp = { "advertiserId": <advertiser_id>, "advertiserName": "<advertiser_name>", "positiveMetricHexCode": "<positive_metric>", "negativeMetricHexCode": "<negative_metric>", "positiveRGB": "<pos_rgb>", "negativeRGB": "<neg_rgb>" }
    Given url 'http://localhost:8080/admin-web-service/tableau/post-campaign-insights/advertiser/color'
    And param advertiserId = <advertiser_id>
    When method Get
    Then status 200
#    * print response
    * match response == resp
    Examples:
      | advertiser_id | advertiser_name           | positive_metric | negative_metric | pos_rgb          | neg_rgb          |
      | 2558145       | Disney                    | #c6bed8         | #c8209d         | rgb(198,190,216) | rgb(200,32,157)  |
      | 4883          | Santander                 | #69dcff         | #ea0404         | rgb(105,220,255) | rgb(234,4,4)     |
      | 1036163       | Honda                     | #ffa1b5         | #fa0000         | rgb(255,161,181) | rgb(250,0,0)     |
      | 397846        | Kellogg's UK - Special K  | #17489b         | #c01f25         | rgb(23,72,155)   | rgb(192,31,37)   |
      | 350166        | Mattel                    | #747577         | #ff3122         | rgb(116,117,119) | rgb(255,49,34)   |
      | 1329633       | Norwegian Air             | #002a37         | #d81939         | rgb(0,42,55)     | rgb(216,25,57)   |
      | 2527153       | Jaguar Land Rover Non-DFA | #000000         | #006635         | rgb(0,0,0)       | rgb(0,102,53)    |
      | 1782847       | Eon                       | #1ea2b1         | #b00402         | rgb(30,162,177)  | rgb(176,4,2)     |
      | 1278468       | Mira Showers              | #1ea2b1         | #000000         | rgb(30,162,177)  | rgb(0,0,0)       |
      | 295829        | Debenhams                 | #db0a5b         | #a3e8d9         | rgb(219,10,91)   | rgb(163,232,217) |
      | 2377942       | Merlin                    | #f2f1c7         | #192a6f         | rgb(242,241,199) | rgb(25,42,111)   |
      | 1927541       | Kellogg's - Crunchy Nut   | #f6c300         | #bf162b         | rgb(246,195,0)   | rgb(191,22,43)   |
      | 282732        | Tourism Ireland           | #0072bc         | #451e77         | rgb(0,114,188)   | rgb(69,30,119)   |
      | 1478471       | Telegraph                 | #8d63a3         | #085f6c         | rgb(141,99,163)  | rgb(8,95,108)    |
      | 3035231       | STX Entertainment         | #eff3f6         | #24293d         | rgb(239,243,246) | rgb(36,41,61)    |
      | 1174849       | Lactalis                  | #ed1b2e         | #0055a5         | rgb(237,27,46)   | rgb(0,85,165)    |


  @AMP-2866
  Scenario Outline: Checking the Default colour for non Custom Adverisers
    * def resp = { "advertiserId": <advertiser_id>, "advertiserName": "##null", "positiveMetricHexCode": "<positive_metric>", "negativeMetricHexCode": "<negative_metric>", "positiveRGB": "<pos_rgb>", "negativeRGB": "<neg_rgb>" }
    Given url 'http://localhost:8080/admin-web-service/tableau/post-campaign-insights/advertiser/color'
    And param advertiserId = <advertiser_id>
    When method Get
    Then status 200
#    * print response
    * match response == resp
    Examples:
      | advertiser_id | positive_metric | negative_metric | pos_rgb         | neg_rgb         |
      | 123           | #f46524         | #27c7bd         | rgb(244,101,36) | rgb(39,199,189) |
      | 321           | #f46524         | #27c7bd         | rgb(244,101,36) | rgb(39,199,189) |
      | 317347        | #f46524         | #27c7bd         | rgb(244,101,36) | rgb(39,199,189) |


  @AMP-2867
  Scenario Outline: The Invalid case for Advertiser_ID by passing string
    Given url 'http://localhost:8080/admin-web-service/tableau/post-campaign-insights/advertiser/color'
    And param advertiserId = "<advertiser_id>"
    When method Get
    Then status 400
    Examples:
      | advertiser_id |
      | abcd          |
      | lkmn          |

  @AMP-2868
  Scenario: Get call to get the PCI Advertisers
    * def adv = { "advertiserId": 4883, "advertiserName": "Santander" }
    Given url 'http://localhost:8080/admin-web-service/tableau/post-campaign-insights/advertisers'
    When method Get
    Then status 200
#    * print response
    * match response contains adv
    * def obj = JSON.parse(response)
    * def adv_len = Object.keys(obj).length
#    * print adv_len
    * match adv_len == 39

  @AMP-2869
  Scenario Outline: Get call to ppt token
    Given url 'http://localhost:8080/admin-web-service/tableau/ppt/token-generator'
    And param userName = "<uname>"
    When method Get
    Then status 200
#    * print response.body
    * match response.body == '#regex.*:.*,,'
    Examples:
      | uname                       |
      | pradhyumna.k@miqdigital.com |
      | 123                         |

  @AMP-2870
  Scenario: Get call to get reach curve channels
    * def channels = [ "Native Video", "Display Standard", "Native Standard", "Display Video", "Facebook Video", "Facebook Standard" ]
    Given url 'http://localhost:8080/admin-web-service/tableau/reach-curve/channel'
    When method Get
    Then status 200
    * match response == channels

  @AMP-2871
  Scenario Outline: Get the Filter values of a user
    Given url 'http://localhost:8080/admin-web-service/tableau/reach-curve/filters'
    And param userName = "<uname>"
    When method Get
    Then status 200
    * print response
    Examples:
      | uname                       |
      | pradhyumna.k@miqdigital.com |
      | neetu.purba@miqdigital.com  |
