@regression1
Feature: Testing lookups

  Background:
    * header Content-Type = 'application/json'
    * header Authorization = 'Bearer eyJhbGciOiJSUzI1NiJ9.eyJhdWQiOlsiaW5maW5pdHkiXSwidXNlcl9uYW1lIjoicHJhZGh5dW1uYS5rQG1pcWRpZ2l0YWwuY29tIiwic2NvcGUiOlsiQURNSU5fREVMRVRFIiwiT1BUSU1JWkVfREVMRVRFIiwiTUFOQUdFX0RFTEVURSIsIkRJU0NPVkVSX0RFTEVURSJdLCJleHAiOjE1NjI5MjQ0ODgsImF1dGhvcml0aWVzIjpbIklORklOSVRZX0FNTkVUX05FV19VU0VSIiwiSU5GSU5JVFlfQU1ORVRfU1VQRVJfQURNSU4iXSwianRpIjoiNjQyNzA2YWEtMmUwZi00NmE3LTgwNzgtZTI0MzlmYzUxZjJiIiwiY2xpZW50X2lkIjoiaW5maW5pdHkifQ.IC7LWmupz8fV-qEH1QLtVmYBUMNnoMS-yjpYdUZNVcHa7t5eGxYjY2IFtdAW49LaWlX3QJLf4O84gyanmSZWQQ5eC0_U7u1542YCwPrKemHZPZQqEP1ZIv5GvYygYkxczqNszOdDMMtxap5Mfaidd7cQsWJl4aAxNvBXrZBj3IFbBRNKnjERrHBn25UhNQsqjiI9dZ1-bwcdchs_lvAoaesbdvtCW9Gj5wqWNdrRKsz1Ud_0n4nWEEUvfjWBqk4zM1WMpi_bfNNxbDD8Wln8bFwTxyYxD7D2vLuNr2KN5TEmX8l0TbUi5M07mL8ljm2XTe41-ksTsJSi4WfkZnX__w'
    * configure ssl = true

  @AMP-1198
  Scenario: Making a Get Call to lookup with valid user
    Given url 'https://dev.amnet-infinity.io/lookups-web-service/api/v1/lookups/'
    And param loggedInUser = 'infinity-admin'
    When method Get
    Then status 200
    * print response

  @AMP-1196
  Scenario: Making a Get Call to lookup with invalid user
    Given url 'https://dev.amnet-infinity.io/lookups-web-service/api/v1/lookups'
    And param loggedInUser = 'ap.l@miq.com'
    When method Get
    Then status 200
    * print response

  @AMP-1199
  Scenario Outline: Making a Get Call to lookup
    Given url 'https://dev.amnet-infinity.io/lookups-web-service/api/v1/lookups/lookup/'
    And param id = <id>
    And param name = '<name>'
    And param recordsStatus = 1;
    When method Get
    Then status 200
    * print response
    Examples:
      | name                               | id |
      | arunan_mediaowner_exrate           | 1  |
      | francis_agency_lookup_new          | 2  |
      | arunan_mediaowner_lookup_apn_deals | 3  |
      | arunan_invsource_lookup            | 4  |
      | francis_advertiser_lookup_new      | 5  |
      | dummy_lookup                       | 6  |


  @AMP-2000
  Scenario Outline: Making a Put Call to lookup
    Given url 'https://dev.amnet-infinity.io/lookups-web-service/api/v1/lookups/lookup/'
    And request <request>
    When method Put
    Then status 200
    * print response
    Examples:
      | request                                                                                                                                                                                                                      |
      | { "data": { "id": 2029, "month": "2017-02-04", "exrate": "34", "updated_date": "12/07/2019", "updated_by_user": "pradhyumna.k@miqdigital.com", "active": 1 }, "recordId": 2029, "tableName": "arunan_mediaowner_exrate" }    |
      | { "data": { "id": 2030, "month": "2017-10-10", "exrate": "22323", "updated_date": "12/07/2019", "updated_by_user": "pradhyumna.k@miqdigital.com", "active": 1 }, "recordId": 2030, "tableName": "arunan_mediaowner_exrate" } |

  @AMP-2001
  Scenario Outline: Making a Get Call to lookup
    Given url 'https://dev.amnet-infinity.io/lookups-web-service/api/v1/lookups/lookup/'
    And request <request>
    When method Post
    Then status 200
    * print response
    Examples:
      | request                                                                                                                                                                                                        |
      | { "data": { "month": "2019-07-02", "exrate": "1.112", "updated_date": "12/07/2019", "updated_by_user": "pradhyumna.k@miqdigital.com", "active": 1 }, "recordId": "", "tableName": "arunan_mediaowner_exrate" } |

  @AMP-2000
  Scenario Outline: Making a Get Call to lookup
    Given url 'https://dev.amnet-infinity.io/lookups-web-service/api/v1/lookups/lookup/'
    And request <request>
    When method Put
    Then status 200
    * print response
    Examples:
      | request                                                                                                                                                                                                                                             |
      | { "data": { "id": 2036, "month": "2019-07-02", "exrate": 1.112, "updated_date": "12/07/2019", "updated_by_user": "pradhyumna.k@miqdigital.com", "active": 0 }, "recordId": 2036, "makeRecordInactive": 1, "tableName": "arunan_mediaowner_exrate" } |

  @AMP-2010
  Scenario Outline: Checking the role for a look up with get call
    Given url 'https://dev.amnet-infinity.io/lookups-web-service/api/v1/lookups/lookup/role/'
    And param lookupId = <id>
    When method Get
    Then status 200
    * print response
    Examples:
      | id |
      | 1  |
      | 2  |
      | 3  |
      | 4  |
      | 5  |
      | 6  |

  @AMP-2116
  Scenario Outline: Checking the role for a look up with get call
    Given url 'https://dev.amnet-infinity.io/lookups-web-service/api/v1/lookups/lookup/role/'
    And request <request>
    When method Put
    Then status 200
    * print response
    Examples:
      | request                                                                                                                      |
      | { "lookupId": 1, "lookupUserRolesResponse": [ { "userId": 170, "userName": "rushyantha@miqdigital.com", "role": "view" } ] } |
      | { "lookupId": 2, "lookupUserRolesResponse": [ { "userId": 39, "userName": "divya-test1", "role": "view" } ] }                |
      | { "lookupId": 3, "lookupUserRolesResponse": [ { "userId": 159, "userName": "divyaa@gmail.com", "role": "admin" } ] }         |
      | { "lookupId": 3, "lookupUserRolesResponse": [ { "userId": 159, "userName": "divyaa@gmail.com", "role": "NULL" } ] }          |
