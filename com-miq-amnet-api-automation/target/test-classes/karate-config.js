function() {
 var env = karate.env;

 //var loc = Java.type('com.mediaiq.caps.activation.dpi.automation.Constant').currentDir;
 //var path1 = loc.split("/target")[0];

 karate.log('karate.env system property was:', env);
 var config = { // base config
            env: env

      };


 if (!env) {
        env = 'local';
        config.baseUrl = 'http://localhost:9119/';
        config.path = 'miqarewards/v1/contact-us';
       }else if (env == 'staging'){
           config.baseUrl = 'http://dev.amnet-infinity.io';
           config.path = "lookups-web-service/api/v1/lookups";
       }

 // don't waste time waiting for a connection or if servers don't respond
    // within 5 seconds
 karate.configure('connectTimeout', 70000);
 karate.configure('readTimeout', 70000);
 return config;
}
